const axios = require("axios").default;

/**
 * Get the total number of issues for a group
 * Get the issue statistics for a group for each label provided
 */

module.exports = config => {
  // adding an `undefined` label so we can get the stats for all issues in a group
    //[...config.labels, undefined].map(async label => {
  return Promise.all(
    [...config.labels].map(async label => {
        return Promise.all (
            ['frontend', 'backend', undefined].map(async specialty => {
            const labels = [
                config.groupLabel,
                specialty,
                label
            ].filter(item => item).join(",");

            const response = await axios({
                method: "get",
                url: `https://gitlab.com/api/v4/issues_statistics?scope=all&labels=${labels}`,
                headers: {
                    //           "PRIVATE-TOKEN": config.gitlabPrivateToken
                    "PRIVATE-TOKEN": process.env.GITLAB_ST_TOKEN
                }
            });

            return {
                specialty: specialty || "any",
                label: label || "all issues",
                statistics: response.data.statistics.counts,
            };

        })
        );
    })
  );
};
